import React from 'react'

const Header = () => {
  return (
    <div style={{padding: '10px 50px', background: 'black'}}>
        <div style={{color: 'white'}}>
           <b> Pizza Customization</b>
        </div>
    </div>
  )
}

export default Header