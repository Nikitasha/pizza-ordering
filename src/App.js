// import './App.css';
import React, { useState, useEffect } from 'react';
import Header from './components/Header';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Customize from './components/Customize';
import Checkout from './components/Checkout';

function App() {
  const [ingredients, setIngredients] = useState({
    basil: false,
    cheese: false,
    mushroom: false,
    olive: false,
    pineapple: false,
    tomato: false,
  })

  useEffect(() => {
    const data = localStorage.getItem("ingredients");
    if (data) {
      setIngredients(JSON.parse(data));
    }
  }, []);

  return (
    <div className="App">

      <Header />

      <BrowserRouter>
        <Routes>
          <Route exact path="/" element={<Customize ingredients={ingredients} setIngredients={setIngredients} />} />
            
          <Route path="/checkout" element={<Checkout ingredients={ingredients} />} />
          
        </Routes>
      </BrowserRouter>

    </div>
  );
}

export default App;
